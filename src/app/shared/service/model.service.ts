import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Model} from 'app/shared/model/model';
import 'rxjs/add/operator/map';  

import {ConnectServer} from'app/shared/service/connectserver.service';


@Injectable()
export class ModelService {
url:String;

  constructor(private _http: Http, private _connect : ConnectServer){
      this.url = this._connect.connect();

  }
  
//recupere les models sous format JSON a partir de l'API
  getModelFromAPI() : Observable <any>{
        return this._http.get(this.url + '/models')
               // .do(x=>console.log(x))
                .map(models => models.json());                       

   }

    getCategorieFromAPIByIdModel(idModel : number) : Observable <any> {
        return this._http
                    .get(this.url + '/categories/' + idModel)
                    .map (
                        categories => categories.json()
                    )
    } 

    getAllCategorieFromAPI() : Observable <any> 
    {
        return this._http
                    .get(this.url+'/categories')
                    .map(
                        categories=>categories.json()
                    )
    }

    createModel(nameModel : string){
        return this._http.post(this.url + '/models',
        {
          name: nameModel
        }
        )
        .map(model => model.json());
    }

    deleteModelByid(idModel : number){
        return this._http.delete(this.url + '/models/' + idModel);
    }
    
    assignModelCategorie(fkModel : number, fkCategory : number){
     return this._http.post(this.url+'/assigncategorytomodel',
     
     {
       fkModel: fkModel,
       fkCategory : fkCategory})
       .map(assign => assign.json());

   }

   createCategory(nameCategory : string){
       return this._http.post(this.url+'/categories',
       {
         name : nameCategory  
       })
       .map(category => category.json());
   }

   createTaskModel(taskName : string, taskDescription : string, categoryName : string, categoryId : number, userType : string){
       return this._http.post(this.url +'/taskmodel', 
       {
          name : taskName,
          description : taskDescription,
          categoryName : categoryName,
          categoryId : categoryId,
          userType : userType,

       })
       .map(taskModel => taskModel.json())
   }

}