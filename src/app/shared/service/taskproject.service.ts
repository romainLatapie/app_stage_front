import { Injectable } from '@angular/core';
import {Http,Response,Headers,RequestMethod,RequestOptionsArgs,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/do'; 
import 'rxjs/add/operator/toPromise'; 
import {ConnectServer} from'app/shared/service/connectserver.service';
import {Categorie,Task} from'app/shared/model/categorie';

@Injectable()
export class TaskProjectService {
url : String;    


constructor(private _http: Http,private _connect : ConnectServer){
    this.url=this._connect.connect();
}

 private _headers = new Headers({'Content-Type': 'application/json'})

createTaskProject(nameTask : string, descriptionTask: string, taskStatut : string, commentaryTask : string, categoryTask : string, creationDate : Date, enDate : Date, fkproject: number, userType : string): Observable <any>{
// console.log("coucou2");

return   this._http.post(this.url + '/tasks', {
        name: nameTask,
       description : descriptionTask,
       statut: taskStatut,
       commentary : commentaryTask,
       category :  categoryTask,
       creationDate : creationDate,
       endDate : enDate,
       fkproject : fkproject,
       userType : userType
    }).map(taskProject =>taskProject.json());
  
}



}
