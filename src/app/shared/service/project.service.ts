import { Injectable } from '@angular/core';
import {Http,Response,Headers,RequestMethod,RequestOptionsArgs,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/do'; 
import 'rxjs/add/operator/toPromise';  
import {ConnectServer} from'app/shared/service/connectserver.service';

import {Project,Categorie,Categories, Task} from'app/shared/model/project';

@Injectable()
export class ProjectService {
url : String;

  constructor(private _http: Http, private _connect : ConnectServer){
    this.url=this._connect.connect();
  }



  headers = new Headers({
            'Content-Type': 'application/json'
        });
  options = new RequestOptions({ method:RequestMethod.Delete,headers: this.headers });

  getAllProjects(){
    return this._http.get(this.url + '/projects')
                     .map(projects => projects.json());

  }

  getProjectFromAPI(projectStatut : string, id : string): Observable<any>{
        return this._http.get(this.url + '/projects/' +id)
                .map(projects => projects.json())
                .map(projectArray => {
                    return projectArray.filter(project=> project.state === projectStatut )});                       

   }

   createProject( nameProject : string, client: number, nameModel : string, startDate : Date, endDate : Date, state: string, creator : string){
     return this._http.post(this.url+'/projects', 
     {
       name: nameProject,
       client : client,
       model: nameModel,
       startDate : startDate,
       endDate :  endDate,
       state: state,
       creator : creator
      })
       .map(projects => projects.json());
   }

   assignProjectUser(fkUser : number, fkProject : number){
     return this._http.post(this.url+'/assign',
     
     {
       fkUser: fkUser,
       fkProject : fkProject})
       .map(assign => assign.json());

   }

   updateProject(projectId : string, nameProject : string, clientProject : number, modelProject: string, startDate : Date, endDate : Date, stateProject : string, creatorProject : string): Observable <any> 
   {
     return this._http.patch(this.url+"/projects/"+projectId, {
       name: nameProject,
       client : clientProject,
       model: modelProject,
       startDate: startDate,
       endDate: endDate,
       state: stateProject,
       creator: creatorProject
     });
   }

   updateTask(taskId: string, nameTask : string, descriptionTask: string, taskStatut : string, commentaryTask : string, categoryTask : string, creationDate : Date, enDate : Date, fkproject: number, userType : string): Observable <any>
   {
     return this._http.patch(this.url+"/tasks/"+ taskId, {
       
       name: nameTask,
       description : descriptionTask,
       statut: taskStatut,
       commentary : commentaryTask,
       category :  categoryTask,
       creationDate : creationDate,
       endDate : enDate,
       fkproject : fkproject,
       userType : userType
});
   }

   deleteTask(taskId : string): Observable<any>{
    return this._http.delete(this.url +"/tasks/"+ taskId);
   } 

}
