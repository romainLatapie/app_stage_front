import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';
//import {Model} from 'app/data/model';
import 'rxjs/add/operator/map';  

import {ConnectServer} from'app/shared/service/connectserver.service';

@Injectable()
export class UserService {
url:String;

  constructor(private _http: Http, private _connect : ConnectServer){
      this.url = this._connect.connect();

  }

  //recupere les users sous format JSON a partir de l'API
  getUserFromAPI() : Observable <any>{
        return this._http.get(this.url + '/users')
               // .do(x=>console.log(x))
                .map(users => users.json());                       

   }

   deletUserFrompAPI(idUser : number) : Observable <any> {
       return this._http.delete(this.url+'/users/'+ idUser);
   }

   createUser(userName : string, lastName: string, userTrigramme : string, login: string, password : string, creationDate: Date, userType: string){
    return this._http.post(this.url+'/users', 
     {
       userName: userName,
       lastName : lastName,
       userTrigramme: userTrigramme,
       login : login,
       password :  password,
       creationDate: creationDate,
       userType : userType
      })
       .map(users => users.json());
   }
}

