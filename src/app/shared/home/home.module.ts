import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppRoutingModule} from 'app/shared/app-routing/app-routing.module';
import { HomeComponent } from './home.component';
import {AuthService} from 'app/shared/service/auth.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule
  ],
  declarations: [
    HomeComponent
    ],
  providers: [AuthService]
})
export class HomeModule { }
