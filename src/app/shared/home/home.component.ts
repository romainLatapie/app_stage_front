import { Component, OnInit } from '@angular/core';
import {User} from 'app/shared/model/users';
import {Router} from '@angular/router';
import {AuthService} from 'app/shared/service/auth.service';


@Component({
  selector: 'num-rec-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 private user: User;
// private submitted : Boolean = false;
 private validation : Boolean = true;


  constructor(private router : Router, private authService : AuthService) { 
    this.user = new User();
   
  }

  ngOnInit() {
    this.authService.logout();
  }
  

public onLogin(value: User) {
  this.authService.login(value.login, value.password)
  .subscribe(
   data => { 
     if(data) { 
      //    console.log(data);
     //   this._token = data.token;
        localStorage.setItem('login',data.login);
        localStorage.setItem('id', data.id);
        localStorage.setItem('name', data.name);
        localStorage.setItem('lastName', data.lastName);
        localStorage.setItem('trigramme', data.trigramme);
        localStorage.setItem('type', data.userType);
        if(localStorage.getItem('type') != 'Admin')
        {
        this.router.navigate(["/currentproject"]);
        }
       else if(localStorage.getItem('type') == 'Admin')
        {
        this.router.navigate(["/projectmanager"]);
        }
    //    this.submitted = true;
        }},
       err=> {
        console.log(err.status);
       this.validation = false;
       }
  );

      
}

}
