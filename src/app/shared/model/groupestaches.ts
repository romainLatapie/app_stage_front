import {GroupeTache} from './groupetache';

export const GROUPETACHE : GroupeTache [] =

[
{"idgroupetache":1,"nom_groupetache":"Transmission"}, 
{"idgroupetache":2,"nom_groupetache":"Production Informatique"}, 
{"idgroupetache":3,"nom_groupetache":"Matières Premières"},
{"idgroupetache":4,"nom_groupetache":"Chargé de Compte"},
{"idgroupetache":5,"nom_groupetache":"Parties Communes"},
{"idgroupetache":6,"nom_groupetache":"Recette Client"},
{"idgroupetache":7,"nom_groupetache":"Editique"},
{"idgroupetache":8,"nom_groupetache":"Lettre Cheque"},
{"idgroupetache":9,"nom_groupetache":"Partie Cheque"},
{"idgroupetache":10,"nom_groupetache":"LCR"},
{"idgroupetache":11,"nom_groupetache":"Titres"},
{"idgroupetache":12,"nom_groupetache":"Gestion des Annexes"},
{"idgroupetache":13,"nom_groupetache":"Gestion des Recommandés"},
{"idgroupetache":14,"nom_groupetache":"Reporting Web"},
{"idgroupetache":15,"nom_groupetache":"Livraison en production"},
{"idgroupetache":16,"nom_groupetache":"Contrôle 1ere production"},
{"idgroupetache":17,"nom_groupetache":"Documentation"},
{"idgroupetache":18,"nom_groupetache":"Integration du document finalisé"},
{"idgroupetache":19,"nom_groupetache":"Specifique Développement"},
{"idgroupetache":20,"nom_groupetache":"Specifique Bureau d'études"},
]