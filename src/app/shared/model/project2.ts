export class Project {
    idproject?: number;
    eid_client?:string;
    eid_type_project?:string;
    projectStatut?:string;
    groupe_tache?: Groupe_Tache[];
}

export class Groupe_Tache {
        nom_groupe_tache: string;
        taches: Tach[];
    }

export class Tach {
        idtaches?: number;
        nom_tache?: string;
        description_tache?: string;
        etat_tache?: string;
        fichier_joint?: string;
        user?: string;
        date?: Date;
        commentaire?: string;
    }   

