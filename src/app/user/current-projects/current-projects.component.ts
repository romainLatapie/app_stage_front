import { Component, OnInit,ElementRef,ViewChild} from '@angular/core';
import {Project,Categorie,Categories, Task, Log, AttachedFile, Client} from'app/shared/model/project';
import {ProjectService} from 'app/shared/service/project.service';
import {Router} from '@angular/router';
import {LogService} from 'app/shared/service/log.service';
import {FileService} from 'app/shared/service/file.service';
import {CreatePdfService} from 'app/shared/service/createpdf.service';
import {GROUPETACHE} from 'app/shared/model/groupestaches';
//import {FilterComponent} from '../current-projects/filter/filter.component';

import { CompleterService, CompleterData } from 'ng2-completer';

//let jsPDF = require('jspdf'); //MAC
//declare let jsPDF;  // WIN



@Component({
  selector: 'num-rec-current-projects',
  templateUrl: './current-projects.component.html',
  styleUrls: ['./current-projects.component.css']
})
export class CurrentProjectsComponent implements OnInit {
private  projects : Project[];
private logs : Log[];
private files: AttachedFile[];
private fileList : AttachedFile[];
private  selectProject: Project;
private  projetselectionne : Project;
private  userId : string = "";
private  tacheselectionne: Task;
private  editajouttache: boolean;
private  editcommentaire: boolean;
private  selectProjectId: number;
private noProject : boolean = false;
//private selectTask : Task = null;




//windows file
@ViewChild('fileInput')
fileInput : ElementRef ;
private taskId : string;
//private selectTask : Task;
  
  //searchTache:string;

//groupetaches = GROUPETACHE;

//groupeTache: string ='';

//autocomplete
projectNameService: CompleterData;
projectName : string;
//taskNameService : CompleterData;
//taskName : string;
//searchTache : string;

private countTask : number;
private countOKTask : number;


 


  constructor(
      private _projectservice : ProjectService, 
      private _logservice : LogService,
      private _completerService : CompleterService, 
      private _fileservice : FileService,
      private router : Router,
      private  _createpdfservice : CreatePdfService) { }

public  ngOnInit() {
    this.getAllTaskonInit();
     
  }

public getAllTaskonInit(){ 
 
        this._projectservice.getProjectFromAPI("actif",localStorage.getItem('id'))
            .subscribe(
              res => 
              {this.projects = res;
            //    console.log(this.projects);
                if(this.projects[0])
                { 
               this.onSelectProject(this.projects[0]); 
               this.projectNameService = this._completerService.local(this.projects,"name","name");
             //  this.countTaskOk(this.selectProject);
                }
                else
                {
                    this.noProject = true;
                }
              },
              err => console.log(err.status)); 
                        
  }

public getAllTask(){ 
 
        this._projectservice.getProjectFromAPI("actif",localStorage.getItem('id'))
            .subscribe(
              res => 
              {this.projects = res;
           //    this.projectNameService = this._completerService.local(this.projects,"name","name");
           //    this.countTaskOk(this.recupSelectProject());
              },
              err => console.log(err.status)); 
                        
  }  

public selectProjectByName()
{
  for (let project of this.projects)
  {
      if( project.name == this.projectName)
      {
          this.onSelectProject(project);
      }
  }
}  

 public onSelectProject(project : Project)
  {
    
   // this.selectProject = Object.assign({},this.selectProject,project);
   this.selectProject = project;
    this.selectProjectId = this.selectProject.id; 
//    this.taskNameService=this._completerService.local(this.selectProject.categorie.categories[0].tasks,"name","name");
  }

public  deleteTask(tache : Task){
    let  idTask: string;
   idTask= this.onSelectTask(tache).id;
   console.log(idTask);
   this._projectservice.deleteTask(idTask).subscribe(
     data=>{
      this.getAllTask();
     }
   )

  }

  public recupSelectProject()
  {
    for (let project of this.projects)
  {
      if( project.id == this.selectProjectId)
      {
          return project;
      }
  }
  }

  public countTaskOk(project : Project)
  {
    this.countTask = 0;
    this.countOKTask = 0;
    for(let categories of project.categorie.categories)
    {
      for (let task of categories.tasks)
      {
        this.countTask++
        if (task.statut == 'ok')
        {
          this.countOKTask++;
        }
      }
    }
    console.log(this.countTask);
    console.log(this.countOKTask);

  }

  updateTaskLog(tache : Task, state : string)
  {
  let idTask : string = this.onSelectTask(tache).id;
  let nameTask: string =this.onSelectTask(tache).name;
  let descriptionTask: string =this.onSelectTask(tache).description;
  let taskStatut : string=state;
  let commentaryTask: string =this.onSelectTask(tache).commentary;
  let categoryTask: string =this.onSelectTask(tache).category;
  let creationDate: Date =this.onSelectTask(tache).creationDate;
  let  endDate = new Date();
  let fkproject: number = this.onSelectTask(tache).fkproject;
  let userType: string=this.onSelectTask(tache).userType;
    this._projectservice.updateTask(idTask,nameTask,descriptionTask, taskStatut, commentaryTask,categoryTask,creationDate,endDate,fkproject,userType ).subscribe(
      data=>{
        this.getAllTask();
      }
    )
    this._logservice.createLogByIdTask(idTask,localStorage.getItem('trigramme'),endDate,state,"").subscribe(
        data=>{
          this.getAllTask();
        }
    ) 
  }

 public  isKo(tache: Task){
   this.updateTaskLog(tache,"ko");  
  }

public  isOk(tache: Task){
  this.updateTaskLog(tache,"ok");

  }

 public isNa(tache : Task){
   if(this.onSelectTask(tache).statut == "na")
   {
    this.updateTaskLog(tache,"");
   }
   else
   {
     this.updateTaskLog(tache,"na");
   } 

  }


 public  onSelect(tache : Task){
    this.tacheselectionne = tache;
  //  console.log(this.tacheselectionne);
  //  console.log(this.tacheselectionne.id);
  //  this.editcommentaire = true;

  }


public onSelectTask(tache : Task ){
  this.tacheselectionne = tache;
  return this.tacheselectionne;
}


public  vueAjouterTache(project: Project){
    this.projetselectionne = project;
    console.log(this.projetselectionne);
    this.editajouttache = true;
    console.log(this.editajouttache);
  }

 //  valideCommentaire(tache : Tach){
public  validCommentary(tache : Task){
  let idTask : string = this.onSelectTask(tache).id;
  let nameTask: string =this.onSelectTask(tache).name;
  let descriptionTask: string =this.onSelectTask(tache).description;
  let taskStatut : string= this.onSelectTask(tache).statut;
  let  commentaryTask : string = tache.commentary ;
  let categoryTask: string =this.onSelectTask(tache).category;
  let creationDate: Date =this.onSelectTask(tache).creationDate;
  let  endDate = new Date();
  let fkproject: number = this.onSelectTask(tache).fkproject;
  let userType: string=this.onSelectTask(tache).userType;
    this._projectservice.updateTask(idTask,nameTask,descriptionTask, taskStatut, commentaryTask,categoryTask,creationDate,endDate,fkproject,userType ).subscribe(
      data=>{
        this.getAllTask();
      }
    ) 
 this._logservice.createLogByIdTask(idTask,localStorage.getItem('trigramme'),endDate,"commentaire",commentaryTask).subscribe(
        data=>{
          this.getAllTask();
        }
    )


  tache.user = localStorage.getItem('trigramme');
  tache.date= new Date();
  // this.hideChildModal();
  console.log(tache.date);
  
 }
 
 public allLog(tache : Task) {
   let idTask : string  = this.onSelectTask(tache).id;
   this._logservice.getLogByIdTask(idTask)
            .subscribe( 
              res => this.logs =res,
              err => console.log(err)
            )
 }

  public viewfiles(tache : Task) {
 //  this.task = tache; 
    let selectTask = this.onSelectTask(tache);
    console.log(selectTask);
    this.taskId = selectTask.id;
    this.fileList = [];
    for(let file of selectTask.attachedFile)
    {
     this.fileList.push(file);
    }
 //   console.log(this.fileList);         
}

addFile()
{
  let fi = this.fileInput.nativeElement;
  let endDate = new Date();
//  let idTask = this.onSelectTask(tache).id;
  let idTask = this.taskId;
  let projectName = this.selectProject.name;
  //console.log(projectName);
 // console.log(fi);
    if (fi.files && fi.files[0]) 
      {
    let fileToUpload = fi.files[0];
    //  console.log(fileToUpload);
      let formData = new FormData();
      
      formData.append('file', fileToUpload, fileToUpload.name);  
      formData.append('filename',fileToUpload.name);
      formData.append('idTask',idTask);
      formData.append('projectname',projectName);
    //  console.log(formData);
      this._fileservice.createFile(formData).subscribe(
        data=>{
        //  console.log(data);
          this.getAllTask();
        }
        );
      this._logservice.createLogByIdTask(idTask,localStorage.getItem('trigramme'),endDate,"fichier",fileToUpload.name).subscribe(
        data=>{
          this.getAllTask();
        }
    )

      }
       
}

createPDF()
{
//  this._createpdfservice.createPDF(this.recupSelectProject);
 
for (let project of this.projects)
  {
      if( project.id == this.selectProjectId)
      {
          this._createpdfservice.createPDF(project);
      }
  }

}

validateProject()
{
  let idProject : string = (this.selectProject.id).toString();
  let nameProject: string =this.selectProject.name;
  let clientProject: number =this.selectProject.client.id;
  let modelProject : string= this.selectProject.model;
  let  startDate : Date = this.selectProject.startDate;
  let  endDate : Date = new Date();;
  let stateProject: string ="fini";
  let creatorProject: string = this.selectProject.creator;
    this._projectservice.updateProject(idProject,nameProject,clientProject,modelProject,startDate,endDate,stateProject,creatorProject).subscribe(
      data=>{
     //   this.getAllTask();
        this.router.navigate(["/finishproject"]);
        this.createPDF();
     //   this._createpdfservice.createPDF(this.selectProject);
      }
    ) 

}




}
