import {Pipe, PipeTransform} from '@angular/core';
//import {Groupe_Tache} from 'app/data/project';
import {Categories} from 'app/shared/model/project';

@Pipe({
   name: 'categoryFilter' 

})

export class CategoryFilterPipe implements PipeTransform{
    /*
        transform( value : Groupe_Tache[], searchCategory: string ='Toutes categories'){
            if (searchCategory !== 'Toutes categories'){
                let result = value.filter((category : Groupe_Tache) => category.nom_groupe_tache.toLocaleLowerCase().includes(searchCategory));
            return result;
         }
         else {
             return value;
         }
        }
        */
        transform( value :Categories[], searchCategory: string ='Toutes categories'){
            if (searchCategory !== 'Toutes categories'){
                let result = value.filter((category : Categories) => category.categoryName.toLocaleLowerCase().includes(searchCategory));
            return result;
         }
         else {
             return value;
         }
        }
}