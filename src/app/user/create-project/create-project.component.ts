import { Component, OnInit } from '@angular/core';
import {AffecModelService} from './affectcategory/affec-model.service';
import {ModelService} from 'app/shared/service/model.service';
import {ClientService} from 'app/shared/service/client.service';
import {UserService} from 'app/shared/service/user.service';
import {TaskProjectService} from 'app/shared/service/taskproject.service';
import {ProjectService} from 'app/shared/service/project.service';
import {Router} from '@angular/router';
import {Model} from 'app/shared/model/model';
import {Client} from 'app/shared/model/client';
import {User} from 'app/shared/model/users';
import {Categorie, Task} from 'app/shared/model/categorie';

import { CompleterService, CompleterData } from 'ng2-completer';

//import {Observable} from 'rxjs/Observable';
//import 'rxjs/Rx';


@Component({
  selector: 'num-rec-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {
  listModels : Model[];
  listClients : Client[];
  listUsers : User [];
  categoriesForCreateTask : Categorie [];
  categoriesselectionnes : Categorie[];
  afficheaffect: boolean ;
  afficheCreaProj: boolean = true;
  afficheUser : boolean ;
  modelSelectName: string;
  modelName:string;
  clientName: string;
  userSelect : User [] = [];
  projectName: string;
  clientId: number;
  taskproject : Task;
  startCreate: boolean ;
  controlInputOk : boolean = false;

  clientNameService: CompleterData;
  modelNameService : CompleterData;
  categorieNameService : CompleterData;

  constructor(
              private _affecmodel: ModelService,
              private _clientservice : ClientService, 
              private _userservice : UserService,
              private _taskprojectservice: TaskProjectService, 
              private _projectservice : ProjectService,
              private router : Router,
              private _completerService : CompleterService) {}

  ngOnInit() {
      this._affecmodel
              .getModelFromAPI()
              .subscribe(
                  res => 
                  {
                    this.listModels = res; 
                   // console.log(this.listModels);
                    this.modelNameService = this._completerService.local(this.listModels,"name","name");

                  },
                      
               //   res => console.log(res),
                  err => console.log(err.status)
      );
      this._clientservice
          .getClientFromAPI()
          .subscribe(
              res => 
              {this.listClients = res;
              this.clientNameService = this._completerService.local(this.listClients,"name","name");
              },
               //   res => console.log(res),
                err => console.log(err.status)
          ) 
     this.startCreate = false;          
             
  }



affectUser(){
  this.afficheCreaProj = false;
  this.afficheUser =  true;
  this._userservice.getUserFromAPI()
              .subscribe(
                res=> this.listUsers = res,
             //   res => console.log(res),
                err => console.log(err.status)
              );
 // console.log(this.listUsers);

}  
 
listenerafficheCreaProjTrue(event){
  this.afficheCreaProj = event;
  
} 

listenerAfficheUserFalse(event){
  this.afficheUser = event;
}

listenerafficheuserselec(userSelect){
  this.userSelect = userSelect;
}

listenerViewCategoryFalse(event){
 this.afficheaffect=event;
}

listenerafficheCategorieSelect(categoriesSelect){
  this.categoriesselectionnes = categoriesSelect;
  this.modelName = this.modelSelectName;
  this.modelSelectName ="";
}

createProject(){
this.startCreate = true;  
for (let client of this.listClients)
{
  if(client.name == this.clientName)
    {
     this.clientId = client.id;
    }
}
let creationdate = new Date();
let endate : Date ;
//console.log(this.userSelect);


this._projectservice.createProject( this.projectName, this.clientId, this.modelName, creationdate, endate, "actif", "romain")
                    .subscribe(
                      project => {
                        for (let categorie of this.categoriesselectionnes)
                            {
                                for( let task of categorie.task)
                                {
                                  

                                  
                              //  console.log(task.name);
                                this._taskprojectservice
                                            .createTaskProject(task.name,task.description,"sans","",task.categorie,creationdate,endate,project.id,task.userType)
                                            .subscribe(
                                              res => this.taskproject = res,
                                              err => console.log(err.status));
                                               
                                }   
                            };
                       
                                    for (let user of this.userSelect) 
                                    {
                                      this._projectservice
                                          .assignProjectUser(user.id,project.id)
                                          .subscribe(
                                            res => {
                                                  console.log(res);
                                                  this.router.navigate(["/currentproject"]);
                                            } ,
                                            err => console.log(err.status)
                                          )
                                    } 
                   
                      },      
                      err => console.log(err.status),
                    ); 
                    


}

controlInput()
{
  if(this.projectName && this.modelName && this.userSelect.length != 0)
  {
    this.controlInputOk = true;
  }
  


}

affectCategory(){
this.afficheaffect = true;
this.afficheCreaProj = false;
let modelSelectionneId: number ;

for ( let model  of this.listModels)
{
  if (model.name == this.modelSelectName)
  {
     modelSelectionneId = model.id;
  } 
}

this._affecmodel
              .getCategorieFromAPIByIdModel(modelSelectionneId)
              .subscribe(
                  res => this.categoriesselectionnes = res,
                    
                //  res => console.log( res),
                  err => console.log(err.status)

      );  

this._affecmodel
              .getCategorieFromAPIByIdModel(modelSelectionneId)
              .subscribe(
                  res => 
                  {
                    this.categoriesForCreateTask = res;
                     this.categorieNameService = this._completerService.local(this.categoriesForCreateTask,"name","name");
                   console.log( res);
                  },
                  err => console.log(err.status)

      );        
  }

}
