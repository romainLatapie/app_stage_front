import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinishProjectsComponent } from './finish-projects.component';
import {NavbarModule} from 'app/shared/navbar/navbar.module';
import {ProjectService} from 'app/shared/service/project.service';

@NgModule({
  imports: [
    CommonModule,
    NavbarModule
  ],
  declarations: [
    FinishProjectsComponent,  
  ],
  providers : [ProjectService],
  exports : []

})
export class FinishProjectsModule { }
