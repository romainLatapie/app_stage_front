import { Component, OnInit } from '@angular/core';
import {ProjectService} from 'app/shared/service/project.service';
//import {Project,Groupe_Tache,Tach} from'app/data/project';
import {Project} from'app/shared/model/project';
import {Router} from '@angular/router';

@Component({
  selector: 'num-rec-finish-projects',
  templateUrl: './finish-projects.component.html',
  styleUrls: ['./finish-projects.component.css']
})
export class FinishProjectsComponent implements OnInit {
  projects : Project [];

  constructor( private _projectservice : ProjectService,private router : Router ) { }

  ngOnInit() {
    this._projectservice.getProjectFromAPI("fini",localStorage.getItem('id'))
        .subscribe(res => this.projects = res,
          err => console.log(err.status));
  }

  reactiveProject(project)
  {
  let idProject : string = (project.id).toString();
  console.log(idProject);
  let nameProject: string =project.name;
  let clientProject: number =project.client.id;
  let modelProject : string= project.model;
  let  startDate : Date = new Date();
  let  endDate : Date = new Date();
  let stateProject: string ="actif";
  let creatorProject: string = project.creator;
    this._projectservice.updateProject(idProject,nameProject,clientProject,modelProject,startDate,endDate,stateProject,creatorProject).subscribe(
      data=>{
     //   this.getAllTask();
        this.router.navigate(["/currentproject"]);
      }
    ) 
  }

}
