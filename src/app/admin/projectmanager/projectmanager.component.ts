import { Component, OnInit } from '@angular/core';
import {ProjectService} from 'app/shared/service/project.service';
import {Project,Categorie,Categories, Task, Log} from'app/shared/model/project';

@Component({
  selector: 'num-rec-projectmanager',
  templateUrl: './projectmanager.component.html',
  styleUrls: ['./projectmanager.component.css']
})
export class ProjectmanagerComponent implements OnInit {
projects : Project [];
  constructor(private projectservice : ProjectService) { }

  ngOnInit() {
    this.projectservice.getAllProjects().subscribe(
      projects => this.projects = projects,
      err => console.log(err.status)
    );
  }

deleteProject(project){}


}
