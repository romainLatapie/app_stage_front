import { Component, OnInit } from '@angular/core';
import {User} from 'app/shared/model/users';
import {UserService } from 'app/shared/service/user.service';

@Component({
  selector: 'num-rec-usermanager',
  templateUrl: './usermanager.component.html',
  styleUrls: ['./usermanager.component.css']
})
export class UsermanagerComponent implements OnInit {


  viewUserManager : boolean = true;
  viewCreateUser : boolean = false;
  viewDeleteUser : boolean = false;
  users : User [];

  constructor(private userservice : UserService) { }

  ngOnInit() {
    this.userservice.getUserFromAPI().subscribe(
      users => this.users = users,
      err => console.log(err.status)
    )
  }

  listenerviewDeleteUserFalse(event){
    this.viewDeleteUser =event;
  }

  listenerviewUserManagerTrue(event){
    this.viewUserManager =event;
  }  

createUser(){
  this.viewUserManager = false;
  this.viewCreateUser = true;

}

deleteUser(){
  this.viewUserManager = false;
  this.viewDeleteUser = true;
}

}
