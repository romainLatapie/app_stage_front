import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import {Model} from 'app/shared/model/model';
import {ModelService} from 'app/shared/service/model.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-deletemodel',
  templateUrl: './deletemodel.component.html',
  styleUrls: ['./deletemodel.component.css']
})
export class DeletemodelComponent  {

@Input()
viewModelManager : boolean;
@Input()
viewDeleteModel : boolean;
@Input()
models : Model[];

@Output() 
viewDeleteModelFalse : EventEmitter<any> = new EventEmitter();
@Output() 
viewModelManagerTrue : EventEmitter<any> = new EventEmitter();

modelSelectName : string ;

  constructor(private modelservice : ModelService, private router : Router) { }

deleteModel(){
  let modelSelectId : number;
  for (let model of this.models)
  {
    if(model.name == this.modelSelectName)
    {
      modelSelectId = model.id;
    }
  }
  this.modelservice.deleteModelByid(modelSelectId).subscribe();
  this.viewModelManager = true;
  this.viewDeleteModel = false;
  this.viewDeleteModelFalse.emit(this.viewDeleteModel);
  this.viewModelManagerTrue.emit(this.viewModelManager);
  this.router.navigate(["/projectmanager"]); 


}

}
