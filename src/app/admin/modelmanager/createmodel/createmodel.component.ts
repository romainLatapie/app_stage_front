import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
import {ModelService} from 'app/shared/service/model.service';
import {Categorie, Task} from 'app/shared/model/categorie';
import {Model} from 'app/shared/model/model';
import {Router} from '@angular/router';

@Component({
  selector: 'num-rec-createmodel',
  templateUrl: './createmodel.component.html',
  styleUrls: ['./createmodel.component.css']
})
export class CreatemodelComponent implements OnInit {

@Input()
viewCreateModel;
@Input()
viewModelManager;

@Output() 
viewCreateModelFalse : EventEmitter<any> = new EventEmitter();
@Output() 
viewModelManagerTrue : EventEmitter<any> = new EventEmitter();

modelName : string;
modelCreated : Model ;
categoriesSelected : Categorie [];
categoriesForCreateTask : Categorie [];
taskCreated : Task;
categorieCreatedId : number;
viewAffectCategory: boolean = false;
startCreate: boolean = false;

  constructor(private _modelservice : ModelService, private router : Router ) { }

  ngOnInit() {
    
  }

listenerviewCategorieFalse(event){
 this.viewAffectCategory=event;
}

listenerviewCreateModelTrue(event){
 this.viewCreateModel=event;
}

listeneroutputCategoriesSelected(categoriesSelect){
  this.categoriesSelected = categoriesSelect;
}

  affectCategory()
  {
    this.viewCreateModel = false;
    this.viewAffectCategory = true;
    this._modelservice.getAllCategorieFromAPI().subscribe(
      res => 
      {
      this.categoriesSelected = res;
 //     console.log(res);
      },    
      err => console.log(err.status)
    )
    this._modelservice
              .getAllCategorieFromAPI()
              .subscribe(
                  res => this.categoriesForCreateTask = res,
                     
                //  res => console.log( res),
                  err => console.log(err.status)

      ); 
  }

  createModel()
  {
   // this.startCreate = true;
    this._modelservice.createModel(this.modelName).subscribe(
          model => {
              this.modelCreated=model;
              for (let categorie of this.categoriesSelected)
                  {
                    if(categorie.newCategory)
                    {
                      this._modelservice.createCategory(categorie.name).subscribe(
                        categorieCreated => {
                                        this._modelservice.assignModelCategorie(this.modelCreated.id,categorieCreated.id)
                                                          .subscribe(
                                                                      assign => console.log(assign),
                                                                        
                                                                       err => console.log(err.status)
                                        
                                                                  );
                                                     
                                      for(let task of categorie.task)
                                            {
                                              if(task.newTask)
                                              {
                                                this._modelservice.createTaskModel(task.name,task.description,task.categorie,categorieCreated.id,"")
                                                                  .subscribe(
                                                                    taskModel => {
                                                                      this.taskCreated = taskModel;
                                                                      
                                                                    },
                                                                    err => console.log(err.status)
                                                                  );
                                              }
                                              
                                            }                                  
                                      },
                                      err=>console.log(err.status)
                      );

                    }
                    else
                       { 
                                  this._modelservice.assignModelCategorie(this.modelCreated.id,categorie.id)
                                                  .subscribe(
                                                              assign => console.log(assign),
                                                                err => console.log(err.status)
                                                              );
                      
                                  for(let task of categorie.task)
                                  {
                                        if(task.newTask)
                                        {
                                          this._modelservice.createTaskModel(task.name,task.description,task.categorie,categorie.id,"")
                                                            .subscribe(
                                                              taskModel =>{
                                                                this.taskCreated = taskModel;
                                                               
                                                              } ,
                                                              err => console.log(err.status)
                                                            );
                                        }

                                  } 
                       }                                            

                  }
    /*       this.startCreate = false;
          this.viewCreateModel=false;
          this.viewModelManager=true;
          this.viewCreateModelFalse.emit(this.viewCreateModel);
          this.viewModelManagerTrue.emit(this.viewModelManager);*/
          this.router.navigate(["/projectmanager"]);               
          },
          err => console.log(err.status)
    );
  }

}
