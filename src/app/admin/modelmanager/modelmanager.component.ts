import { Component, OnInit } from '@angular/core';
import {Model} from 'app/shared/model/model';
import {ModelService} from 'app/shared/service/model.service';

@Component({
  selector: 'num-rec-modelmanager',
  templateUrl: './modelmanager.component.html',
  styleUrls: ['./modelmanager.component.css']
})
export class ModelmanagerComponent implements OnInit {

  viewModelManager : boolean = true;
  viewCreateModel : boolean = false;
  viewDeleteModel : boolean = false;
  models : Model [];

  constructor(private _modelservice : ModelService) { }

  ngOnInit() {
    this._modelservice.getModelFromAPI().subscribe(
        models => this.models = models,
        err => console.log(err.status)
    )
  }

  listenerviewCreateModelFalse(event){
    this.viewCreateModel=event;
  }

  listenerviewModelManagerTrue(event){
    this.viewModelManager=event;
  }

  listenerviewDeleteModelFalse(event){
    this.viewDeleteModel=event;
  }



  createModel(){
    this.viewModelManager = false;
    this.viewCreateModel = true;

  }

  deleteModel(){
    this.viewModelManager = false;
    this.viewDeleteModel = true;
  }

}
